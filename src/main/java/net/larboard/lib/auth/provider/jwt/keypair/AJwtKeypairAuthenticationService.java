package net.larboard.lib.auth.provider.jwt.keypair;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.larboard.lib.auth.service.AAuthenticationService;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.time.Instant;
import java.util.Base64;

@Slf4j
public abstract class AJwtKeypairAuthenticationService extends AAuthenticationService {
    private static final Base64.Decoder decoder = Base64.getDecoder();

    private static final KeyFactory keyFactory;
    static {
        try {
            keyFactory = KeyFactory.getInstance("RSA");
        }
        catch (NoSuchAlgorithmException e) {
            log.error(e.getMessage(), e);
            throw new ExceptionInInitializerError(e);
        }
    }

    private final RestTemplate restTemplate;

    private PublicKey publicKey;

    private JwtPublicKeyData jwtPublicKeyData;

    private final String jwtKeyHost;
    private final String jwtKeyUrl;
    private final String jwtIssuer;

    public AJwtKeypairAuthenticationService(@NonNull IJwtKeypairAuthConfig jwtAuthConfig, @NonNull RestTemplate restTemplate) {
        this.restTemplate = restTemplate;

        jwtKeyHost = jwtAuthConfig.getJwtKeyHost();
        jwtKeyUrl = jwtAuthConfig.getJwtKeyUrl();
        jwtIssuer = jwtAuthConfig.getJwtIssuer();
    }

    @Override
    public String getSignerName() {
        return jwtIssuer;
    }

    private synchronized void checkPublicKey() {
        if(jwtPublicKeyData == null || jwtPublicKeyData.getExpiryTime() <= Instant.now().getEpochSecond()) {
            renewPublicKey();
        }
    }

    private synchronized void renewPublicKey() {
        log.debug("renewing public key");

        jwtPublicKeyData = retrieveKey();

        if (jwtPublicKeyData == null || jwtPublicKeyData.getPublicKey() == null) {
            throw new AuthenticationServiceException("jwtPublicKeyData or jwtPublicKeyData.getPublicKey() is null");
        }

        try {
            val keySpec = new X509EncodedKeySpec(
                    decoder.decode(
                            jwtPublicKeyData.getPublicKey()
                    )
            );

            publicKey = keyFactory.generatePublic(keySpec);
        }
        catch (InvalidKeySpecException e) {
            log.error(e.getMessage(), e);
            throw new AuthenticationServiceException("Retrieved public key is invalid.", e);
        }
    }

    private JwtPublicKeyData retrieveKey() {
        try {
            val res = restTemplate.getForEntity(jwtKeyHost+jwtKeyUrl, JwtPublicKeyData.class);

            if(res.hasBody()) {
                return res.getBody();
            }

            log.error("!hasBody()");
            throw new AuthenticationServiceException("Error retrieving public key.");
        }
        catch (RestClientException e) {
            log.error(e.getMessage(), e);
            throw new AuthenticationServiceException("Error retrieving public key.", e);
        }
    }

    public User authenticateWithToken(String headerToken) {
        return checkSystemToken(headerToken, false);
    }

    private User checkSystemToken(String headerToken, boolean isRetryAttempt) {
        try {
            checkPublicKey();

            val claims = Jwts.parser()
                    .setSigningKey(publicKey)
                    .setAllowedClockSkewSeconds(1)
                    .parseClaimsJws(headerToken)
                    .getBody();

            return processUser(claims, headerToken);
        }
        catch (ExpiredJwtException e) {
            log.debug("auth err: {}", e.getMessage());
            throw new CredentialsExpiredException(e.getMessage(), e);
        }
        catch (JwtException e) {
            log.debug("auth err: {}", e.getMessage());
            if(!isRetryAttempt) {
                renewPublicKey();
                return checkSystemToken(headerToken, true);
            }
            throw new BadCredentialsException(e.getMessage(), e);
        }
    }
}