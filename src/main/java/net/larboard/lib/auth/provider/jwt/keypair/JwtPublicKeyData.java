package net.larboard.lib.auth.provider.jwt.keypair;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JwtPublicKeyData {
    private String publicKey;

    private Long expiryTime;
}
