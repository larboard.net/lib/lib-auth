package net.larboard.lib.auth.provider.google;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.larboard.lib.auth.service.AAuthenticationService;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.User;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@Slf4j
public abstract class AGoogleAuthenticationService extends AAuthenticationService {
    private static final JsonFactory jacksonFactory = new GsonFactory();

    private static final NetHttpTransport netHttpTransport = new NetHttpTransport();

    private final String googleClientId;
    protected final String trustedDomainName;

    public AGoogleAuthenticationService(@NonNull IGoogleAuthConfig googleAuthConfig) {
        googleClientId = googleAuthConfig.getGoogleClientId();
        trustedDomainName = googleAuthConfig.getGoogleTrustedDomainName();
    }

    public User authenticateWithToken(String headerToken) {
        return checkGoogleToken(headerToken);
    }

    private User checkGoogleToken(String headerToken) {
        val verifier = new GoogleIdTokenVerifier.Builder(netHttpTransport, jacksonFactory)
                .setAudience(Collections.singletonList(googleClientId))
                .build();

        try {
            val idToken = verifier.verify(headerToken);
            if (idToken != null) {
                val payload = idToken.getPayload();

                return processUser(new DefaultClaims(payload), headerToken);
            }
            else {
                throw new BadCredentialsException("Invalid ID token.");
            }
        }
        catch (GeneralSecurityException | IOException e) {
            throw new AuthenticationServiceException(e.getMessage(), e);
        }
    }
}