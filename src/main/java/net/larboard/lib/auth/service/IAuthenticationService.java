package net.larboard.lib.auth.service;

import org.springframework.security.core.userdetails.User;

public interface IAuthenticationService {
    User authenticateWithToken(String headerToken);

    String getSignerName();
}
