package net.larboard.lib.auth.service;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import lombok.NonNull;
import lombok.Setter;
import lombok.val;
import net.larboard.lib.auth.security.AuthenticationHeaderToken;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
public class HeaderTokenAuthenticationService {
    private static final Base64.Decoder decoder = Base64.getDecoder();

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private static final JavaType mapType;
    static {
        val typeFactory = objectMapper.getTypeFactory();
        mapType = typeFactory.constructMapType(HashMap.class, typeFactory.constructType(String.class), typeFactory.constructType(Object.class));
    }

    private final Map<String, IAuthenticationService> authenticationServices = new HashMap<>();

    @Setter
    private String defaultProvider = null;

    public void register(@NonNull IAuthenticationService authenticationService) {
        val provider = authenticationService.getSignerName();

        if(provider == null || provider.isEmpty()) {
            throw new IllegalArgumentException("Provider key must not be null or empty.");
        }

        authenticationServices.put(provider, authenticationService);
    }

    public User authenticate(Authentication authentication) {
        if(authentication == null) {
            throw new AuthenticationCredentialsNotFoundException("Authentication object is null.");
        }

        val token = (AuthenticationHeaderToken)authentication;

        var provider = retrieveIssuerClaim((String)token.getCredentials());

        if(!authenticationServices.containsKey(provider) && defaultProvider != null) {
            provider = defaultProvider;
        }

        if(provider == null || !authenticationServices.containsKey(provider)) {
            throw new ProviderNotFoundException(String.format("Provider not in list: '%s'", provider));
        }

        return authenticationServices.get(provider).authenticateWithToken((String)token.getCredentials());
    }

    private String retrieveIssuerClaim(String token) {
        val split = token.split("\\.");

        if(split.length != 3) {
            throw new AuthenticationServiceException("not a valid jwt (dots)");
        }

        Map<String, String> decoded;

        try {
            decoded = objectMapper.readValue(decoder.decode(split[1]), mapType);
        }
        catch (IOException ex) {
            throw new AuthenticationServiceException("failed to decode claims", ex);
        }

        if(!decoded.containsKey(Claims.ISSUER)) {
            return null;
        }

        return decoded.get(Claims.ISSUER);
    }
}
