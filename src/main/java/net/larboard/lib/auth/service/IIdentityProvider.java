package net.larboard.lib.auth.service;

public interface IIdentityProvider<ID> {
    ID getAccountId();

    boolean isSignedIn();

    boolean hasAuthority(final String authority);
}
